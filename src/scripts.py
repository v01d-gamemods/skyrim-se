from trainerbase.gameobject import GameObject
from trainerbase.scriptengine import AbstractBaseScript, ScriptEngine

from objects import endurance, hp, mana


regeneration_script_engine = ScriptEngine(0.05)


def create_regeneration_script(game_object: GameObject) -> AbstractBaseScript:
    @regeneration_script_engine.simple_script
    def script():
        if game_object.value < 0:
            game_object.value += 5

    return script


better_hp_regen = create_regeneration_script(hp)
better_mana_regen = create_regeneration_script(mana)
better_endurance_regen = create_regeneration_script(endurance)
