from trainerbase.memory import Address
from trainerbase.process import pm


player_stats_address = Address(pm.base_address + 0x01EC47C8)
player_parameters_address = Address(pm.base_address + 0x2EFF7D8, [0xF0, 0x50, 0x30])
muting_footsteps_address = pm.base_address + 0x1DD85F0
weapon_attack_distance_address = pm.base_address + 0x1DE0BB0
