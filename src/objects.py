from trainerbase.gameobject import GameFloat

from memory import (
    muting_footsteps_address,
    player_parameters_address,
    player_stats_address,
    weapon_attack_distance_address,
)


hp = GameFloat(player_stats_address + [0x210])
mana = GameFloat(player_stats_address + [0x21C])
endurance = GameFloat(player_stats_address + [0x228])

max_carry_weight = GameFloat(player_parameters_address + [0x50])
invisibility = GameFloat(player_parameters_address + [0x100])
muting_footsteps = GameFloat(muting_footsteps_address)

weapon_attack_speed = GameFloat(player_parameters_address + [0x130])
weapon_damage_multiplier = GameFloat(player_parameters_address + [0x148])
weapon_attack_distance = GameFloat(weapon_attack_distance_address)
